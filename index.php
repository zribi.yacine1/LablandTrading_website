<?php
if(isset($_POST['submit'])){
    $to = "contact@lablandtrading.com.tn"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $subject = $_POST['subject'];
    $subject2 = "Copy of your form submission";
    $message = $first_name . " " . $last_name . " a écrit ce qui suit:" . "\n\n" . "Nom: " . $first_name . "\n" . "Prénom: " . $last_name . "\n" . "Objet: " . $subject . "\n\n" . "Message: " . "\n" . $_POST['message'];
    $message2 = "Voici une copie de votre message " . "\n\n" . "Nom: " . $first_name . "\n" . "Prénom: " . $last_name . "\n" . "Objet: " . $subject . "\n\n" . "Message: " . "\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    }
?>

<!doctype html>
<!--
COPYRIGHT by HighHay/Mivfx
Before using this theme, you should accept themeforest licenses terms.
http://themeforest.net/licenses
-->

<html class="no-js" lang="en">


<!-- Mirrored from highhay.com/demos/comet/index-style5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Aug 2017 22:04:36 GMT -->

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- Page Title Here -->
    <title>Labland Trading s.a.r.l</title>
    <meta name="theme-color" content="#73BF45" />

    <!-- Page Description Here -->
    <meta name="description"
        content="A beautiful and creative portfolio template. It is mobile friend (responsive) and comes with smooth animations">

    <!-- Disable screen scaling-->
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, user-scalable=0">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->

    <!-- Web fonts and Web Icons -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="icon.ico" />
    <link rel="stylesheet" href="fonts/opensans/stylesheet.css">
    <link rel="stylesheet" href="fonts/montserrat/stylesheet.css">
    <!-- <link rel="stylesheet" href="fonts/roboto/stylesheet.css"> -->
    <link rel="stylesheet" href="fonts/ionicons.min.css">
    <link rel="stylesheet" href="css/Features-Boxed.css">


    <!-- Vendor CSS style -->
    <link rel="stylesheet" href="css/pageloader.css">

    <!-- Uncomment below to load individualy vendor CSS -->
    <link rel="stylesheet" href="css/foundation.min.css">
    <!-- <link rel="stylesheet" href="js/vendor/swiper.min.css"> -->
    <link rel="stylesheet" href="js/vendor/jquery.fullpage.min.css">
    <!-- <link rel="stylesheet" href="js/vegas/vegas.min.css"> -->

    <!-- Main CSS files -->
    <link rel="stylesheet" href="css/main.css">
    <!-- alt layout -->
    <link rel="stylesheet" href="css/style-color5.css">

    <!-- <script src="js/vendor/modernizr-2.7.1.min.js"></script> -->
</head>

<body id="menu" class="hh-body alt-bg left-light">
    <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Page Loader -->
    <div class="page-loader" id="page-loader">
        <div>
            <div class="icon ion-spin"></div>
            <p>loading</p>
        </div>
    </div>

    <!-- BEGIN OF site header Menu -->
    <header class="hh-header header-top">
        <!-- Begin of logo -->
        <div class="logo-wrapper">
            <a href="#">
                <h2 class="logo">
                    <span class="logo-img">
                        <img class="light-logo" src="img/logo_dark.png" alt="Logo">
                    </span>

                    <span class="logo-text">
                        <span class="title"><img class="light-logo" src="img/Sans titre.jpg" alt="Logo"></span>
                        <!--<span class="desc">Our Startup</span>-->
                    </span>
                </h2>
            </a>
        </div>
        <!-- End of logo -->

        <!-- Begin of menu icon -->
        <a class="menu-icon">
            <div class="bars">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
        </a>
        <!-- End of menu icon -->

        <!-- Begin of menu -->
        <nav class="menu-links">
            <ul class="links">
                <li><a href="#Acceuil">Acceuil</a></li>
                <li><a href="#Partenaires">Partenaires</a></li>
                <li><a href="#Catalogues">Catalogues</a></li>
                <li><a href="#Contact">Contact</a></li>
                
                <!--<li class="cta"><a href="https://themeforest.net/user/mivfx/portfolio" target="_blank">Buy now</a></li>-->
            </ul>
        </nav>
        <!-- End of menu -->
    </header>
    <!-- END OF site header Menu-->

    <!-- BEGIN OF page cover -->
    <div class="hh-cover page-cover">
        <!-- Cover Background -->
        <div class="cover-bg pos-abs full-size bg-img  bg-blur-0" data-image-src="img/bg-default5.jpg"></div>

        <!-- Transluscent mask as filter -->
        <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(27,27,27,0.50)"></div>

        <!-- Line mask as filter -->
        <div class="cover-bg-mask pos-abs full-size bg-line"></div>

    </div>
    <!--END OF page cover -->

    <!--&lt;!&ndash; BEGIN OF clock container &ndash;&gt;-->
    <!--<div class="clock-container">-->
    <!--&lt;!&ndash; Coutdown Clock &ndash;&gt;-->
    <!--<div class="clock clock-countdown">-->
    <!--<div class="site-config"-->
    <!--data-date="01/01/2018 00:00:00" -->
    <!--data-date-timezone="+0"-->
    <!--&gt;</div>-->
    <!--<div class="clock-wrapper">-->
    <!--<div class="title">-->
    <!--<h3>New website here in</h3>-->
    <!--</div>-->
    <!---->
    <!--<div class="clock-hms">-->
    <!--<div class="tile tile-days">-->
    <!--<span class="days">00</span>-->
    <!--<span class="txt">days</span>-->
    <!--</div>-->
    <!---->
    <!--<div class="tile tile-hours">-->
    <!--<span class="hours">00</span>-->
    <!--<span class="txt">hours</span>-->
    <!--</div>-->
    <!--<div class="tile tile-minutes">-->
    <!--<span class="minutes">00</span>-->
    <!--<span class="txt">minutes</span>-->
    <!--</div>-->
    <!--<div class="tile tile-seconds">-->
    <!--<span class="seconds">00</span>-->
    <!--<span class="txt">seconds</span>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--&lt;!&ndash; END OF clock container &ndash;&gt;-->


    <!-- BEGIN OF page main content -->
    <main class="page-main fullpg" id="mainpage">

        <!-- Begin of home section -->
        <div class="section section-home fp-auto-height-responsive" data-section="Acceuil">
            <div class="content">

                <!-- Begin of centered Content -->
                <section class="c-centered anim">
                    <div class="wrapper">

                        <!-- Title and description -->
                        <div class="title-desc">
                            <div class="t-wrapper">
                                <!-- Logo -->
                                <!--<div class="logo home-logo ">
                                    <img class="" src="img/logo.png" alt="Logo">
                                </div>-->
                                <!-- Title -->
                                <header class="title">
                                    <h2>Quelle est <strong>Labland Trading</strong>?</h2>
                                    <h3>Labland trading est une entreprise créée en 2017 spécialisée dans la
                                        distribution des équipements scientifiques et réactifs destinés aux
                                        utilisateurs dans les différents secteurs de la recherche scientifique.</h3>
                                </header>
                                <!-- desc -->
                                <!--<div class="desc">-->
                                <!--<p>Ever wanted to impress your audience at first view? Use this unique and beautiful website template.</p>-->
                                <!--</div>-->
                            </div>
                        </div>

                        <!-- Action button -->
                        <!--<div class="cta-btns">-->
                        <!--<a class="btn rect-btn btn-inv"  href="#about-us">-->
                        <!--<span class="txt">About us</span>-->
                        <!--<span class="arrow-icon"></span>-->
                        <!--</a>-->
                        <!---->
                        <!--<a class="btn rect-btn" target="_blank"  href="https://themeforest.net/user/mivfx/portfolio">-->
                        <!--&lt;!&ndash;<span class="txt">Buy now</span>&ndash;&gt;-->
                        <!--&lt;!&ndash;<span class="arrow-icon"></span>&ndash;&gt;-->
                        <!--</a>-->
                        <!--</div>-->

                        <!--<div class="clock-container-small"></div>-->

                    </div>
                </section>
                <!-- End of centered Content -->
            </div>
        </div>
        <!-- End of home section -->

        <!-- Begin of about us section -->
        <!--<div class="section section-about section-cent fp-auto-height-responsive" data-section="about-us">-->
        <!---->
        <!--<section class="content clearfix">-->
        <!--&lt;!&ndash; content title &ndash;&gt;-->
        <!--&lt;!&ndash;<div class="c-title">-->
        <!--<h2>Our company</h2>-->
        <!--</div>&ndash;&gt;-->
        <!---->
        <!--&lt;!&ndash; left elements &ndash;&gt;-->
        <!--<div class="c-left anim">-->
        <!--<div class="wrapper">-->
        <!--&lt;!&ndash; title and description &ndash;&gt;-->
        <!--<header class="title-desc">-->
        <!--<h3 class="title">About Us</h3>-->
        <!--<h2>Business</h2>-->
        <!--<p>Do not spend your valuable time building website from scratch. Instead, use this template as the framework of your next beautiful website.</p>-->
        <!--</header>-->
        <!---->
        <!--&lt;!&ndash; Action button &ndash;&gt;-->
        <!--<div class="cta-btns">-->
        <!--<a class="btn arrow-circ-btn"  href="#services">-->
        <!--<span class="txt">Services</span>-->
        <!--<span class="arrow-icon"></span>-->
        <!--</a>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--&lt;!&ndash; end of left elements &ndash;&gt;-->
        <!---->
        <!--&lt;!&ndash; right elements &ndash;&gt;-->
        <!--<div class="c-right">-->
        <!--&lt;!&ndash; title and texts wrapper&ndash;&gt;-->
        <!--<div class="wrapper">-->
        <!--&lt;!&ndash; title and description &ndash;&gt;-->
        <!--<div class="title-desc">-->
        <!--<h4>Everything you should know</h4>-->
        <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet malesuada feugiat. Curabitur fermentum bibendum nulla, non dictum ipsum tincidunt non. Quisque convallis pharetra tempor. Donec id pretium leo. Pellentesque luctus massa non elit viverra pellentesque. Cras vitae neque molestie, rhoncus ipsum sit amet, lobortis dui. Fusce in urna sem. Vivamus vehicula dignissim augue et scelerisque. Etiam quam nisi, molestie ac dolor in, tincidunt tincidunt arcu. Praesent sed justo finibus, fringilla velit quis, porta erat. Donec blandit metus ut arcu iaculis iaculis. Cras nec dolor fringilla justo ullamcorper auctor. Aliquam eget pretium velit. Morbi urna justo, pulvinar id lobortis in, aliquet placerat orci.</p>-->
        <!---->
        <!--&lt;!&ndash;<div class="illustr">-->
        <!--<img src="/img/bg-default4.jpg" alt="Illustration">-->
        <!--</div>&ndash;&gt;-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--&lt;!&ndash; end of right elements &ndash;&gt;-->
        <!--</section>-->
        <!--</div>-->
        <!-- End of about us section -->

        <!-- Begin of services section -->
        <!--<div class="section section-services fp-auto-height-responsive" data-section="services">-->
        <!---->
        <!--<section class="content clearfix">-->
        <!---->
        <!--&lt;!&ndash; right elements &ndash;&gt;-->
        <!--<div class="c-left anim">-->
        <!--<div class="wrapper">-->
        <!--&lt;!&ndash; title &ndash;&gt;-->
        <!--<header class="title-desc">-->
        <!--<h2>Services</h2>-->
        <!--<p>Yes, we can do that all at once.</p>-->
        <!--</header>-->
        <!---->
        <!--&lt;!&ndash; Action button &ndash;&gt;-->
        <!--<div class="cta-btns">-->
        <!--<a class="btn arrow-circ-btn"  href="#contact">-->
        <!--<span class="txt">Contact Us</span>-->
        <!--<span class="arrow-icon"></span>-->
        <!--</a>-->
        <!--</div>-->
        <!--</div>-->
        <!--</div>-->
        <!--&lt;!&ndash; end of right elements &ndash;&gt;-->
        <!---->
        <!--&lt;!&ndash; left elements &ndash;&gt;-->
        <!--<div class="c-right">-->
        <!--<div class="wrapper">-->
        <!--&lt;!&ndash; Begin of features/services/offers &ndash;&gt;-->
        <!--<ul class="item-list row small-up-1 medium-up-2 large-up-2">-->
        <!--&lt;!&ndash; item &ndash;&gt;-->
        <!--<li class="column">-->
        <!--<div class="item-desc">-->
        <!--<h3>Painting</h3>-->
        <!--<div class="desc">-->
        <!--<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</li>-->
        <!--&lt;!&ndash; item &ndash;&gt;-->
        <!--<li class="column">-->
        <!--<div class="item-desc">-->
        <!--<h3>Design</h3>-->
        <!--<div class="desc">-->
        <!--<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</li>-->
        <!--&lt;!&ndash; item &ndash;&gt;-->
        <!--<li class="column">-->
        <!--<div class="item-desc">-->
        <!--<h3>Attorney</h3>-->
        <!--<div class="desc">-->
        <!--<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</li>-->
        <!--&lt;!&ndash; item &ndash;&gt;-->
        <!--<li class="column">-->
        <!--<div class="item-desc">-->
        <!--<h3>Restaurant</h3>-->
        <!--<div class="desc">-->
        <!--<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum a sit amet nulla.</p>-->
        <!--</div>-->
        <!--</div>-->
        <!--</li>-->
        <!--</ul>-->
        <!--&lt;!&ndash; End of features/services/offers &ndash;&gt;-->
        <!--</div>-->
        <!--</div>-->
        <!--&lt;!&ndash; end of left elements &ndash;&gt;-->
        <!---->
        <!--</section>-->
        <!---->
        <!--</div>-->
        <!-- End of services section -->

        <!--Begin of Works/projects section-->
        <div class="section section-projects fp-auto-height-responsive" data-section="Partenaires">
            <section class="content clearfix">

                <!-- left elements -->
                <div class="c-left anim">
                    <!-- title and texts wrapper-->
                    <div class="wrapper">
                        <!-- title -->
                        <header class="title-desc">
                            <h2 class="title page-title">Nos Partenaires</h2>
                        </header>

                    </div>
                </div>
                <!-- end of left elements -->

                <!-- left elements -->
                <div class="c-right">
                    <div class=" clip-overflow">
                        <!-- Begin of works/services/products -->
                        <div class="slider-container">
                            <!-- pagination -->
                            <!--<div class="items-pagination"></div>-->
                            <!--<div class="items-nav-container">-->
                            <!--<div class="items-button items-button-prev"><a>Previous</a></div>-->
                            <!--<div class="items-button items-button-next"><a>Next</a></div>-->
                            <!--</div>-->
                            <!-- slider -->

                            <!-- <div class="auto-slider" id="slider">
                                <ul class="auto-slider__content">
                                  <li><img src="http://lorempixel.com/output/food-q-c-720-360-1.jpg" alt="food-1"></li>
                                  <li><img src="http://lorempixel.com/output/food-q-c-720-360-2.jpg" alt="food-2"></li>
                                  <li><img src="http://lorempixel.com/output/food-q-c-720-360-3.jpg" alt="food-3"></li>
                                  <li><img src="http://lorempixel.com/output/food-q-c-720-360-4.jpg" alt="food-4"></li>
                                </ul>
                              </div> -->

                            <!-- <div onmouseover="stopSlider()" onmouseout="continueSlider()">
                                <div class="row">
                                    <div class="column">
                                        <div class="mySlides fade" href="http://www.abcr.de" onclick="window.open('http://www.abcr.de'); return false;">
                                            <img src="img/abcr.png" width="460">
                                        </div>

                                        <div class="mySlides fade">
                                            <img src="img/1.png" width="460">
                                        </div>

                                        <div class="mySlides fade">
                                            <img src="img/2.png" width="200">
                                        </div>



                                    </div>
                                </div>
                            </div> -->

                            <div class="carousel slide" data-ride="carousel" id="carousel-1" data-interval="1700">
                                <div class="carousel-inner" role="listbox" style="
                                height: 200px;
                            ">
                                    <div class="carousel-item active"><img class="w-20 " src="img/abcr.png" width="400"
                                            onclick="window.open('http://www.abcr.de'); return false;"></div>
                                    <div class="carousel-item"><img class="w-20 " src="img/1.png" width="400"
                                            onclick="window.open('https://www.berndkraft.de/en'); return false;"></div>
                                    <div class="carousel-item"><img class="w-20 " src="img/2.png" width="200"
                                            onclick="window.open('http://en.lorenz-glasinstrumente.de/'); return false;">
                                    </div>
                                    <div class="carousel-item"><img class="w-20" src="img/brand.jpg" width="150"
                                            onclick="window.open('https://www.brand.de/fr/laboratory-instruments-and-consumables/'); return false;">
                                    </div>
                                    <div class="carousel-item"><img class="w-20" src="img\disera.png" width="300"
                                            onclick="window.open('http://www.disera.com.tr/'); return false;"></div>
                                    <div class="carousel-item"><img class="w-20" src="img\biochem.png" width="500"
                                            onclick="window.open('https://www.biochemsrl.it/en'); return false;"></div>
                                </div>
                                <div><a class="carousel-control-prev" href="#carousel-1" role="button"
                                        data-slide="prev"><span class="carousel-control-prev-icon"></span><span
                                            class="sr-only">Previous</span></a><a class="carousel-control-next"
                                        href="#carousel-1" role="button" data-slide="next"><span
                                            class="carousel-control-next-icon"></span><span
                                            class="sr-only">Next</span></a></div>
                                <!-- <ol class="carousel-indicators">
                                    <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-1" data-slide-to="1"></li>
                                    <li data-target="#carousel-1" data-slide-to="2"></li>
                                </ol> -->
                            </div>





                        </div>
                        <!-- End of works/services/products -->
                    </div>
                </div>
                <!-- end of left elements -->
            </section>
        </div>
        <!-- End of Works/projects section -->





















        <div class="section section-home fp-auto-height-responsive" data-section="Catalogues">
            <div class="content">

                <!-- Begin of centered Content -->
                <section class="c-centered anim">
                    <div class="wrapper">

                        <!-- Title and description -->
                        <div class="title-desc">
                            <div class="t-wrapper">
                                <!-- Logo -->
                                <!--<div class="logo home-logo ">
                                        <img class="" src="img/logo.png" alt="Logo">
                                    </div>-->
                                <!-- Title -->
                                <br>
                                <br>
                                <header class="title">
                                    <h2><strong>Nos catalogues</strong></h2>
                                </header>
                                <div class="features-boxed">
                                    <div class="container">
                                        <!-- <div class="intro">
                                                <h2 class="text-center">Features </h2>
                                                <p class="text-center">Nunc luctus in metus eget fringilla. Aliquam sed justo ligula. Vestibulum nibh erat, pellentesque ut laoreet vitae.</p>
                                            </div> -->
                                        <div class="row justify-content-center features">
                                            <div class="col-sm-6 col-md-5 col-lg-3 item">
                                                <div class="box centre-image"><img
                                                        class="slider-container centre-image polariod "
                                                        src="img/abcr.png">

                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-5 col-lg-3 item" onclick="window.open('https://drive.google.com/file/d/1gKK0HMDXdOOXjrGTWr2ueo1r_OdYB32R/view'); return false;">
                                                <div class="box centre-image"><img class="slider-container polariod"
                                                        src="img/1.png">

                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-5 col-lg-3 item">
                                                <div class="box centre-image"><img class="polariod" src="img/2.png"
                                                        width="100">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-5 col-lg-3 item">
                                                <div class="box centre-image"><img class="polariod" src="img/brand.jpg"
                                                        width="80">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-5 col-lg-3 item">
                                                <div class="box centre-image" style="
                                                        height: 133px;
                                                    "><img class="polariod" src="img\disera.png">

                                                </div>
                                            </div>

                                            <div class="col-sm-6 col-md-5 col-lg-3 item">
                                                <div class="box centre-image" style="
                                                    height: 133px;
                                                "><img class="polariod" src="img\biochem.png">

                                                </div>
                                            </div>

                                            <!-- C:\Users\dell\Desktop\lablandtradingwebsite\img\brand.jpg -->
                                        </div>
                                    </div>
                                </div>
                                <!-- desc -->
                                <!--<div class="desc">-->
                                <!--<p>Ever wanted to impress your audience at first view? Use this unique and beautiful website template.</p>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End of centered Content -->
            </div>
        </div>













        <!-- Begin of contact section -->
        <div class="section section-contact fp-auto-height-responsive hide-clock" data-section="Contact">

            <section class="content clearfix">

                <!-- Begin of  left elements -->
                <div class="c-left anim">
                    <div class="wrapper contact-wrapper">
                        <!-- title -->
                        <header class="title-desc">
                            <h2 class="page-title">Contactez-nous</h2>
                            <p>Besoin d'aide ou simplement voulez-vous dire bonjour?</p>
                            <div class="column anim">
                                <div class="item-desc">
                                    <h3 class="title">Adresse postale</h3>
                                    <div class="desc">
                                        <!--<p>-->
                                        <!--Route de Gabes km 8-->
                                        <!--<br>3083 Sfax Tyna - Tunisie-->
                                        <!--&lt;!&ndash;<br>South Est, Antartica&ndash;&gt;-->
                                        <!--</p>-->
                                        <p>
                                            Boite postale numéro 141 - 3083 Sfax Tyna - Tunisie
                                            <!--<br>South Est, Antartica-->
                                        </p>
                                    </div>

                                    <h3 class="title">Contact</h3>
                                    <div class="desc">
                                        <p>
                                            Tél : (+216) 52 664 255
                                            <br> Fax : (+216) 74 664 099
                                            <br>Email : <a style="font-weight:bold"
                                                href="#">contact@lablandtrding.com.tn</a>
                                            <!--<br> <p style="font-size: 14px"> </p>-->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </header>

                        <!-- Begin of contact list -->
                        <!--/<div class=" c-features row small-up-1 medium-up-1 large-up-2">-->
                        <!-- item -->


                        <!--</div>-->
                        <!-- End of contact list -->


                    </div>
                </div>
                <!-- End of left elements -->


                <!-- Begin of right elements -->
                <div class="c-right">
                    <div class="wrapper c-form">
                        <!-- begin of contact form content -->
                        <div class="c-content card-wrapper">
                            <form class="message form send_message_form" method="post" id="message_form">
                                <div class="form-header clearfix">
                                    <h3>
                                        Écrivez-nous
                                    </h3>
                                    <div class="btns ">
                                        <button id="submit-message" class="btn arrow-circ-btn email_b" name="submit"
                                            value="Submit>
                                            <span class=" txt">Envoyer</span>
                                            <span class="arrow-icon"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="fields clearfix">
                                    <!-- <div class="input name"> -->
                                    <div class="input bottom">
                                        <label for="mes-nom">
                                            <p style="font-size: 8px"></p>Prénom :
                                        </label>
                                        <input id="mes-nom" name="last_name" type="text" placeholder=""
                                            class="form-success-clean" required style="padding-left: 80px">
                                    </div>
                                </div>
                                <div style="height:10px;"><br></div>
                                <div class="fields clearfix">
                                    <div class="input bottom">
                                        <label for="mes-prenom">
                                            <p style="font-size: 8px"></p>Nom :
                                        </label>
                                        <input id="mes-prenom" name="first_name" type="text" placeholder=""
                                            class="form-success-clean" required>
                                    </div>
                                </div>
                                <div style="height:10px;"><br></div>
                                <div class="fields clearfix">
                                    <div class="input bottom">
                                        <label for="mes-prenom">
                                            <p style="font-size: 8px"></p>Email :
                                        </label>
                                        <input id="mes-email" name="email" type="email" placeholder=""
                                            class="form-success-clean" required>
                                    </div>
                                </div>
                                <div style="height:10px;"><br></div>
                                <div class="fields clearfix">
                                    <div class="input bottom">
                                        <label for="mes-subject">
                                            <p style="font-size: 8px"></p>Objet :
                                        </label>
                                        <input id="mes-subject" type="text" placeholder="" name="subject"
                                            class="form-success-clean" required>
                                    </div>
                                </div>
                                <div style="height:20px;"><br></div>
                                <div class="fields clearfix no-border">
                                    <label for="mes-text">
                                        <p style="font-size: 8px"></p>Message
                                    </label>
                                    <textarea id="mes-text" placeholder="..." name="message" class="form-success-clean"
                                        required></textarea>

                                    <div>
                                        <p class="message-ok invisible form-text-feedback form-success-visible">Your
                                            message has been sent, thank you.</p>
                                    </div>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6LdAFpMUAAAAAAKUZy7z1WA9bTwz19Axi2J6jYZc"></div>
                            </form>

                        </div>
                        <!-- end of contact form content -->
                    </div>
                </div>
                <!-- End of right elements -->
            </section>

        </div>
        <!-- End of contact section -->













    </main>

    <!-- BEGIN OF page footer -->
    <footer id="site-footer" class="site-footer">
        <!-- social networks -->
        <!--<div class="contact">-->
        <!--<ul class="socials">-->
        <!--<li><a class="circ-btn" href="http://facebook.com/highhay"><i class="icon ion-social-facebook"></i></a></li>-->
        <!--<li><a class="circ-btn" href="http://twitter.com/miradontsoa"><i class="icon ion-social-twitter"></i></a></li>-->
        <!--</ul>-->
        <!--</div>-->

        <!-- Notes -->
        <div class="note">
            <p> &copy; 2019 Labland Trading </p>
        </div>

        <!-- subscription form -->
        <!--<div class="subscription">-->
        <!--<h3 class="title">Subscribe to Newsletter : </h3>-->
        <!--&lt;!&ndash; Begin Ajax subscription form  subscription-form &ndash;&gt;-->
        <!--<form id="subscription-form" class="form send_email_form" method="post" action="http://highhay.com/demos/comet/ajaxserver/serverfile.php">-->
        <!--<p class="feedback gone form-success-visible">Thank you for your subscription. We will inform you.</p>-->
        <!--<input id="reg-email" class="input form-success-invisible" name="email" type="email" required placeholder="your@email.address" data-validation-type="email">-->
        <!--<button id="submit-email" class="btn circ-btn form-success-invisible" name="submit_email">-->
        <!--<span class="ion-checkmark"></span>-->
        <!--</button>-->
        <!--</form>-->
        <!--&lt;!&ndash; End of Ajax subscription form  subscription-form &ndash;&gt;-->

        <!--&lt;!&ndash; Begin Native MailChimp Signup Form mc_embed_signup &ndash;&gt;-->
        <!--&lt;!&ndash;<div id="mc_embed_signup">-->
        <!--<form  action="//yebbiz.us3.list-manage.com/subscribe/post?u=6f9aaebaea67b078fc33b4f01&amp;id=02235833b4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>-->
        <!--<div id="mc_embed_signup_scroll">-->

        <!--<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>-->
        <!--<div class="mc-field-group">-->
        <!--<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>-->
        <!--</label>-->
        <!--<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">-->
        <!--</div>-->
        <!--<div id="mce-responses" class="clear">-->
        <!--<div class="response" id="mce-error-response" style="display:none"></div>-->
        <!--<div class="response" id="mce-success-response" style="display:none"></div>-->
        <!--</div>-->
        <!--<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6f9aaebaea67b078fc33b4f01_02235833b4" tabindex="-1" value=""></div>-->
        <!--<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>-->
        <!--</div>-->
        <!--</form>-->
        <!--</div>&ndash;&gt;-->
        <!--&lt;!&ndash;End Native MailChimp Signup Form mc_embed_signup&ndash;&gt;-->
        <!--</div>-->


        <!-- Arrows Scroll down arrow -->
        <!-- Move it next to fp nav using javascript -->
        <div class="s-footer scrolldown">
            <a class="down btn">
                <span class="text">Défiler</span>
            </a>
        </div>
        <!-- End of Scroll down arrow -->
    </footer>
    <!-- END OF site footer -->



    <script>
        var slideIndex = 0;

        var isSliderActivated = true;

        var stopSlider = () => {
            this.isSliderActivated = false;
        }

        var continueSlider = () => {
            this.isSliderActivated = true;
        }

        var showSlides = () => {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            if (this.isSliderActivated) {
                for (i = 0; i < slides.length; i++) {
                    slides[i].style.display = "none";
                }
                slideIndex++;
                if (slideIndex > slides.length) { slideIndex = 1 }

                slides[slideIndex - 1].style.display = "block";
            }

            //dots[slideIndex-1].className += " active";
            setTimeout(showSlides, 2000); // Change image every 2 seconds
        }
        showSlides();
    </script>


    <!-- scripts -->
    <!-- All Javascript plugins goes here -->
    <!--		<script src="//code.jquery.com/jquery-1.12.4.min.js"></script>-->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <!-- <script src="js/jquery.min.js"></script> -->
    <!-- All vendor scripts -->
    <script src="js/vendor/scrolloverflow.min.js"></script>
    <script src="js/vendor/all.js"></script>
    <!-- <script src="js/particlejs/particles.min.js"></script> -->


    <!-- Detailed vendor scripts : for manual update -->
    <!--<script src="./js/vendor/swiper.min.js"></script>
    <script src="./js/vendor/scrolloverflow.min.js"></script>
    <script src="./js/vendor/jquery.fullPage.min.js"></script>
    <script src="./js/vegas/vegas.min.js"></script>
    <script src="./js/vendor/jquery.maximage.js"></script>-->

    <!-- Countdown script -->
    <!-- <script src="js/jquery.downCount.js"></script> -->

    <!-- Form script -->
    <!--<script src="js/form_script.js"></script>-->

    <!-- Javascript main files -->
    <script src="js/main.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <script src="https://www.google.com/recaptcha/api.js?render=6LdAFpMUAAAAAAKUZy7z1WA9bTwz19Axi2J6jYZc"></script>
    <!-- <script>
    grecaptcha.ready(function() {
        grecaptcha.execute('6LdAFpMUAAAAAICY6crGZ5T5bG_lr4W4_igojGu7', {action: 'homepage'}).then(function(token) {
           alert("reCAPTCHA")
        });
    });
    </script> -->


    <!-- Google Analytics: Uncomment and change UA-XXXXX-X to be your site"s ID. -->
    <!--<script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src="//www.google-analytics.com/analytics.js";
            r.parentNode.insertBefore(e,r)}(window,document,"script","ga"));
            ga("create","UA-XXXXX-X");ga("send","pageview");
        </script>-->


    <!-- Uncomment below to enable particles scripts -->
    <!--<script src="./js/particlejs/particles-init.js"></script>-->
    <!--<script src="./js/particlejs/particles-init-snow.js"></script>-->
    <!--    <script src="./js/particlejs/particles-init.js"></script>-->

</body>


<!-- Mirrored from highhay.com/demos/comet/index-style5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 08 Aug 2017 22:04:55 GMT -->

</html>